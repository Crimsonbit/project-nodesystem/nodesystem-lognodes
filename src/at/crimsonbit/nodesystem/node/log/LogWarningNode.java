package at.crimsonbit.nodesystem.node.log;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.util.log.NSLogger;

public class LogWarningNode extends AbstractNode {

	@NodeInput
	Object logText;

	@Override
	public void compute() {
		NSLogger.getLogger("Log Node").warn(logText);
	}
}
