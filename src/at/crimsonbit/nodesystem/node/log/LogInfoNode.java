package at.crimsonbit.nodesystem.node.log;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.util.log.NSLogger;

public class LogInfoNode extends AbstractNode {

	@NodeInput
	Object logText;

	@Override
	public void compute() {
		NSLogger.getLogger("Log Node").info(logText);
	}
}
