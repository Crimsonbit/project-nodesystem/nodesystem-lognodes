package at.crimsonbit.nodesystem.node.log;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

public enum Log implements INodeType {
	INFO("Log Info"), SEVERE("Log Severe"), WARNING("Log Warning");

	private String name;

	private Log(String s) {

		this.name = s;
	}

	@Override
	public String nodeName() {
		return this.name;
	}

}
