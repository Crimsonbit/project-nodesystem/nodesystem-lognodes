package at.crimsonbit.nodesystem.node.module;

import at.crimsonbit.nodesystem.node.log.Log;
import at.crimsonbit.nodesystem.node.log.LogInfoNode;
import at.crimsonbit.nodesystem.node.log.LogSevereNode;
import at.crimsonbit.nodesystem.node.log.LogWarningNode;
import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class LogNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(Log.INFO, LogInfoNode.class);
		registry.registerDefaultFactory(Log.WARNING, LogWarningNode.class);
		registry.registerDefaultFactory(Log.SEVERE, LogSevereNode.class);

	}

}
